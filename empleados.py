#!/usr/bin/env python3

import unittest

class Persona:
	def __init__(self, n, d):
		self.nombre = n
		self.nif = d

class Empleado(Persona):
	"""Un ejemplo de clase para Empleados"""
	def __init__(self, n, d, s):
		super().__init__(n, d)
		self.nomina = s

		if self.nomina < 0:
			self.nomina = 0
		else:
			self.nomina = s

	def calculo_impuestos (self):
		return self.nomina*0.30
	
	def __str__(self):
		return "El empleado {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre, nif = self.nif, tax=self.calculo_impuestos())

class Jefe(Empleado):

	def __init__(self, n, d, s, extra= 0):
		super().__init__(n, d, s)
		self.bonus = extra

	def calculo_impuestos1 (self):
		return (self.nomina + self.bonus)*0.30

	def calculo_impuestos (self):
		return super().calculo_impuestos() + self.bonus*0.30

	def __str__(self):
		return "La jefa {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre, nif = self.nif, tax=self.calculo_impuestos())

class TestEmpleado(unittest.TestCase):

	def test_construir(self):
		e1 = Empleado("nombre", "nif", 5000)
		self.assertEqual(e1.nomina, 5000)

	def test_impuestos(self):
		e1 = Empleado("nombre", "nif", 5000)
		self.assertEqual(e1.calculo_impuestos(), 1500)

	def test_str(self):
		e1 = Empleado("pepe","nif", 50000)
		self.assertEqual("El empleado pepe con NIF nif debe pagar 15000.00", e1.__str__())

class TestJefe(unittest.TestCase):

	def test_construir(self):
		j1 = Jefe("nombre", "nif", 30000, 2000)
		self.assertEqual(j1.nomina, 30000, 2000)

	def test_impuestos(self):
		j1 = Jefe("nombre", "nif", 30000, 2000)
		self.assertEqual(j1.calculo_impuestos(), 9600)

	def test_str(self):
		j1 = Jefe("ana", "nif", 30000, 2000)
		self.assertEqual("La jefa ana con NIF nif debe pagar 9600.00", j1.__str__())


if __name__ == "__main__":
	unittest.main()

