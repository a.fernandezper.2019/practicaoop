#!/usr/bin/env python3

import unittest
from empleado import Empleado
from jefe import Jefe

class TestEmpleado(unittest.TestCase):

	def test_construir(self):
		e1 = Empleado("nombre", "nif", 5000)
		self.assertEqual(e1.nomina, 5000)

	def test_impuestos(self):
		e1 = Empleado("nombre", "nif", 5000)
		self.assertEqual(e1.calculo_impuestos(), 1500)

	def test_str(self):
		e1 = Empleado("pepe","nif", 50000)
		self.assertEqual("El empleado pepe con NIF 3948 debe pagar 15000.00", e1.__str__())

class TestJefe(unittest.TestCase):

	def test_construir(self):
		j1 = Jefe("nombre", "nif", 30000, 2000)
		self.assertEqual(j1.nomina, 30000, 2000)

	def test_impuestos(self):
		j1 = Jefe("nombre", "nif", 30000, 2000)
		self.assertEqual(j1.calculo_impuestos(), 9600)

	def test_str(self):
		j1 = Jefe("ana", "nif", 30000, 2000)
		self.assertEqual("La jefa ana con NIF 3947 debe pagar 9600.00", j1.__str__())


if __name__ == "__main__":
	unittest.main()
