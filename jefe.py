#!/usr/bin/env python3

import unittest
from empleado import Empleado

class Jefe(Empleado):

	def __init__(self, n, d, s, extra= 0):
		super().__init__(n, d, s)
		self.bonus = extra

	def calculo_impuestos1 (self):
		return (self.nomina + self.bonus)*0.30

	def calculo_impuestos (self):
		return super().calculo_impuestos() + self.bonus*0.30

	def __str__(self):
		return "La jefa {name} con NIF 3947 debe pagar {tax:.2f}".format(name=self.nombre, nif = self.nif, tax=self.calculo_impuestos())